// Bootstrap Grid System Components (row, col)
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function Banner ({isValidRoute}) {
  return (
    <Row>
      {isValidRoute ? (
        <Col className="p-5">
          <h1>Zuitt Coding Bootcamp</h1>
          <p>Opportunities for everyone, everywhere.</p>
          <Button variant="primary">Enroll Now!</Button>
        </Col>
      ) : (
        <Col className="p-5">
          <h1>Page Not Found</h1>
          <p>Go back to the <Link as={ Link } to="/">homepage.</Link></p>
        </Col>
      )}
    </Row>   
  )
}