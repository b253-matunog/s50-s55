// import { useState, useEffect } from 'react';
// import PropTypes from "prop-types"; 
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CourseCard ({ course }) {

  // Checks to see if the data was successfully passed
  // console.log(props);
  // Every component receives information in a form of an object
  // console.log(typeof props);

  const { _id, name, description, price } = course;

  // Use the state hook for this component to be able to store its state
  // States are used to keep track of information related to individual components
  // Syntax
  // const [getter, setter] = useState(initialGetterValue);

  // const [ seats, setSeats ] = useState(30);
  // const [ count, setCount ] = useState(0);
 
  // // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
  // // console.log(useState(0));
  // function enroll () {
  //   if (seats > 0) {
  //     setSeats(seats - 1);
  //     setCount(count + 1);
  //   } else {
  //     alert("No more seats.");
  //   }
      
  // }

  // // Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
  // // This is run automatically both after initial render and for every DOM update
  // // Checking for the availability for enrollment of a course is better suited here
  // // [seats] is an OPTIONAL parameter
  // // React will re-run this effect ONLY if any of the values contained in this array has changed from the last render / update
  // useEffect(() => {
  //   if (seats === 0) {
  //     setSeats(false);
  //   }
  // },[seats]);

  return (

    <Card className="cardHighlight my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Link className="btn btn-primary" to={`/courses/${_id}`} >Details</Link>
      </Card.Body>
    </Card>

  )
}

// CourseCard.PropTypes = {

//   courseProp: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })

// }